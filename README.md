# OnForm: Deriving Web Forms from Ontologies
### Steps to run the OnForm web app locally:
1) First, setup local SPARQL endpoints:
    1) An endpoint providing data of [WordNet](http://wordnet-rdf.princeton.edu/about "WordNet") is expected at:
       - <http://localhost:5820/wordnet/query>
    2) An endpoint providing data of Basisklassifikation is expected at:
       - <http://localhost:5820/basisklassifikation/query>
       - Basisklassifikation triples are located in `ontoform/tools/basisklassifikation.nt`
2) Set `DEBUG = True` in `adaptiveUI/settings.py` to run the application locally
3) Activate the virtual environment, located in `venv/`
4) Run the application, e.g. `manage.py runserver`
5) By default, the web form is available at: <localhost:8000/registration>
6) Submitted data is stored in `ontoform/rdf/submission_data/`
