from django import forms


# used for validation
class SubmissionForm(forms.Form):
    # citational section
#    identifier = forms.URLField(required=False)
    title = forms.CharField()
    creator = forms.CharField()
    description = forms.CharField()
    license = forms.CharField(required=False)
    publisher = forms.CharField(required=False)
    publicationYear = forms.IntegerField()

    # context
    research_area = forms.CharField(required=False)
    data_type = forms.CharField(required=False)
    keywords = forms.CharField(required=False)

