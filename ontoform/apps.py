import os

from django.apps import AppConfig
from adaptiveUI.settings import BASE_DIR


class OntoformConfig(AppConfig):
    name = 'ontoform'

    SPARQL_ENDPOINTS = {
        'wikidata': 'https://query.wikidata.org/sparql',
        'dbpedia': 'http://dbpedia.org/sparql',
        'wordnet': 'http://localhost:5820/wordnet/query',
        'basisklassifikation': 'http://localhost:5820/basisklassifikation/query',
    }
    
    NAMESPACES = {
        'onf': 'http://purl.org/net/vsr/onf#',
        'onf_survey': 'http://purl.org/net/vsr/onf/desc/survey#',
        'list': 'http://www.co-ode.org/ontologies/list.owl#'
    }

    SUBMISSION_DIR = os.path.join(BASE_DIR, 'ontoform/rdf/submission_data/')
    ONF_ONTO_DIR = os.path.join(BASE_DIR, 'ontoform/rdf/')
    ONF_DESC_DIR = os.path.join(BASE_DIR, 'ontoform/rdf/descriptions/')
    CONTEXTS_DIR = os.path.join(BASE_DIR, 'ontoform/contexts/')
