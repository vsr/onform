function _fill_out_form() {
    $('#title').val("Input Title");
    $('#description').val("Input Description");
    $('#license').val("Input License");
    $('#publicationYear').val("2020-06-01");
    $('#context_research_area').val("http://uri.gbv.de/terminology/bk/54");
    $('#context_data_type').val("Tabular Data");
    let uri = "http://dbpedia.org/resource/Standard_test_tone";
    $('#context_keywords').append(
        '<option name="' + "test" + '" value="' + uri + '" selected></option>'
    );
}

// remove tag from hidden select
function remove_tag(tag_name) {
    $('[name="' + tag_name + '"]').remove();
}

function add_keyword(item) {
    let $entered_keywords = $('#entered_keywords');
    let $context_keywords_input = $('#context_keywords_input');

    // hide 'select a keyword'-prompt
    $('#keyword_feedback').css('display', 'none');

    // add new keyword
    let new_keyword;
    //   a) user selected "Not Found" entry:
    if (item.uri === '') {
        new_keyword = $('#context_keywords_input').val();
        if (!$entered_keywords.tagExist(new_keyword)) {
            $entered_keywords.addTag(new_keyword);
            $('#context_keywords').append(
                '<option name="' + new_keyword + '" value="' + new_keyword + '" selected></option>'
            );
        }
    //   b) user selected suggestion
    } else if (!$entered_keywords.tagExist(item.label)) {
        $entered_keywords.addTag(item.label);
        $('#context_keywords').append(
            '<option name="' + item.label + '" value="' + item.uri + '" selected></option>'
        );
    }

    // empty input element
    $context_keywords_input.val('');
    $context_keywords_input.typeahead('val', '');
}

function empty_inputs() {
    $('#context_research_area').val('');
    $('#context_data_type').val('');
    $('#context_keywords').val('');
    $('#entered_keywords').val('');
}

function init_context_research_area() {
    // Constructs the suggestion engine
    let suggestions = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('label'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: research_areas
    });

    // Initializing the typeahead
    let $context_research_area_input = $('#context_research_area_input');
    $context_research_area_input.typeahead(null, {
        name: 'research_areas',
        source: suggestions,
        display: 'label',
        highlight: true,
        limit: 10 /* Specify max number of suggestions to be displayed */
    });

    // event listeners
    $context_research_area_input.on('typeahead:autocompleted', function(evt, item) {
        $('#context_research_area').val(item.uri);
        $('#context_research_area_input').removeClass('is-invalid');
    });
    $context_research_area_input.on('typeahead:selected', function(evt, item) {
        $('#context_research_area').val(item.uri);
        $('#context_research_area_input').removeClass('is-invalid');
    });
}

function init_context_keywords() {
    // immediate input of keywords is forbidden
    $('#entered_keywords_tag').attr('readonly', 'readonly');

    // get available keywords for current input
    let keywords = new Bloodhound({
        datumTokenizer: function(datum) {
            return Bloodhound.tokenizers.obj.whitespace(datum.label);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        limit: 10,
        remote : {
            url: API_ENTITYLOOKUP + '?label=%QUERY',
            wildcard: '%QUERY',
            filter: function(data) {
                if(data.results.bindings.length) {
                    return $.map(data.results.bindings, function(obj) {
                        return {
                            uri: obj.uri.value,
                            label: obj.label.value,
                            description: obj.description.value
                        };
                    });
                } else {
                    return [
                            {
                                uri: '',
                                label: $('#context_keywords_input').val(),
                                description: 'No results found for "' + $('#context_keywords_input').val() + '".<br>Click to add anyway.'
                            }
                    ];
                }
            }
        }
    });

    // Initializing the typeahead
    let $context_keywords_input = $('#context_keywords_input');
    $context_keywords_input.typeahead(null, {
        name: 'keywords',
        display: 'label',
        source: keywords.ttAdapter(),
        limit: 10,
        templates: {
            suggestion: function(data) {
                return '<div><strong>' + data.label + '</strong><p><small>' + data.description + '</small></p></div>';
            }
        },
    });

    // setup #entered_keywords below the keywords input field
    $('#entered_keywords').tagsInput({
        'onRemoveTag': remove_tag,
        'height':'',
        'width':'',
        'defaultText': ''
    });

    // event listeners
    $context_keywords_input.on('keyup', function(e) {
        if(e.key === 'Enter') {
            // select first suggestion
            let $first_suggestion = $('.tt-dataset-keywords .tt-selectable').first();
            let $keyword_feedback = $('#keyword_feedback');

            // no suggestion available -> do not add random keyword!
            if($first_suggestion.length < 1) {
                $keyword_feedback.css('display', 'block');
                return;
            }
            $keyword_feedback.css('display', 'none');

            $first_suggestion.trigger('click');
        }
    });
    $context_keywords_input.on('typeahead:autocomplete', function(evt, item) {
        add_keyword(item);
        $('#context_keywords_input').removeClass('is-invalid');
    });
    $context_keywords_input.on('typeahead:selected', function(evt, item) {
        add_keyword(item);
        $('#context_keywords_input').removeClass('is-invalid');
    });
}

function append_add_repeatable_btns() {
    // remove old menus
    $('.dropdown').each(function() {
        $(this).remove();
    });

    // append new add buttons
    let btn_counter = 0;
    $('.o2f-repeatable').each(function() {
        btn_counter++;
        let $repeatable = $(this);
        let $repeatable_parent = $(this).parent();
        let group_name_uri = $(this).attr('o2f-group-name');
        let ontology_name = $('#root').attr('ontology-name');
        let repeatable_name = $(this).find('h2').first().text();
        let $edit_menu = $(' <div class="dropdown">' +
            '<button class="edit-btn btn btn-default btn-sm" data-toggle="dropdown" aria-haspopup="true"><i class="fa fa-edit" aria-hidden="true"></i></button>' +
            '<div class="dropdown-menu">' +
            '</div>' +
            '</div>');

        let $add_btn = $('<button id="add-btn-' + btn_counter + '" class="dropdown-item add-btn btn btn-secondary btn-sm"><i class="fa fa-plus-circle"></i> Add new ' + repeatable_name + '</button>');
        let $del_btn = $('<button id="del-btn-' + btn_counter + '" class="dropdown-item del-btn btn btn-secondary btn-sm"><i class="fa fa-minus-circle"></i> Delete ' + repeatable_name + '</button>');

        $add_btn.on('click', function(e) {
            e.preventDefault();
            fetch(API_REPEATABLE + '?uri=' + encodeURIComponent(group_name_uri) +
                '&ontology_name=' + encodeURIComponent(ontology_name), {
                headers: {
                    'Content-type': 'html'
                }
            })
                .then(response => response.text())
                .then((response) => {
                    $repeatable_parent.append(response);
                    append_add_repeatable_btns();
                    // add event listeners to add values as RDFa
                    $('input[property]').on('change', function() {
                        $(this).attr('content', $(this).val());
                    });
                    // activate popover
                    $('[data-toggle="popover"]').popover({trigger: 'focus'});
                });
        });

        $del_btn.on('click', function(e) {
            e.preventDefault();
            $repeatable.remove();
        });

        // append btns to dropdown
        $edit_menu.find('.dropdown-menu').append($add_btn);
        $edit_menu.find('.dropdown-menu').append($del_btn);

        $(this).find('div.row').first().append($edit_menu);
    });
}

function submit_registration_form() {
    $('#registration-form').off('submit');
    $('#registration-form').submit(function(e) {
        e.preventDefault();
        let data = new FormData();
        let name, val;

        $('input[name]').each(function() {
            name = $(this).attr('name');
            val = $(this).val();

            if(name !== 'context_research_area')
                data.append(name, val);
        });

        data.append('research_area', $('#context_research_area').val());
        data.append('data_type', $('#context_data_type').val());
        data.append('keywords', $('#context_keywords').val());
        data.append('o2f_description', $('#registration-form').html());
        data.append('csrfmiddlewaretoken', $('input[name="csrfmiddlewaretoken"]').val());

        fetch(API_SUBMISSION, {
            method: 'POST',
            body: data
        })
        .then(response => {
            if (!response.ok) {
                 response.json().then(error_list => {
                     append_error_list(error_list);
                     throw new Error('Missing inputs.\n' + JSON.stringify(error_list));
                 });
            } else {
                window.location = '/success';
            }

            return false;
        });
    });
}

function is_context_input() {
    let is_input = true;
    let research_area = $('#context_research_area').val();
    let data_type = $('#context_data_type').val();
    let keywords = $('#context_keywords').val();
    let error_list = {};

    if(research_area === '') {
        $('#context_research_area_input').addClass('is-invalid');
        is_input = false;
        error_list['Research Area'] = 'This field is required';
    }
    if(data_type === null) {
        $('#context_data_type').addClass('is-invalid');
        error_list['Data Type'] = 'This field is required';
        is_input = false;
    }
    if(keywords.length === 0) {
        $('#context_keywords_input').addClass('is-invalid');
        error_list['Keywords'] = 'This field is required';
        is_input = false;
    }

    if(!is_input)
        append_error_list(error_list);

    return is_input;
}

function build_form(e) {
    e.preventDefault();

    // if context is missing, do nothing
    if(!is_context_input())
        return;

    let data = new FormData();
    data.append('research_area', $('#context_research_area').val());
    data.append('data_type', $('#context_data_type').val());
    data.append('keywords', $('#context_keywords').val());
    data.append('csrfmiddlewaretoken', $('input[name="csrfmiddlewaretoken"]').val());
    document.cookie = "csrftoken="+$('input[name="csrfmiddlewaretoken"]').val();
    fetch(API_BUILDFORM, {
        method: 'POST',
        body: data
    })
    .then(response => {
        return response.text();
    })
    .then(response => {
        let $form = $('form').first();
        let $submit_btn = $('#submit-btn');

        $form.append(response);
        $form.append($submit_btn);

        $submit_btn.off('click');
        $submit_btn.text('Submit');
        $submit_btn.on('click', function() {
            submit_registration_form();
        });
        // activate popover
        $('[data-toggle="popover"]').popover({trigger: 'focus'});
        // add event listeners to add values as RDFa
        $('input[property]').on('change', function() {
            $(this).attr('content', $(this).val());
        });
        // add '+' button for repeatables
        append_add_repeatable_btns();
        $('#error-list').remove();
    });
}

function append_error_list(error_list) {
    let $error_list = $('<ul id="error-list"></ul>');

    for(let key in error_list)
        $error_list.append('<li><strong>' + key + '</strong>' + ': ' + error_list[key] + '</li>');

    $('#error-list').remove();
    $("#submit-btn").before($error_list);
}

$(function() {
    empty_inputs();
    init_context_research_area();
    init_context_keywords();
    $('#context_data_type').on('change', function() {
        $(this).removeClass('is-invalid');
    });

    $('[data-toggle="popover"]').popover({trigger: 'focus'});
    $('.twitter-typeahead').css('display','block');

    $('#submit-btn').on('click', function(e){
        build_form(e);
    });

    $('.yearpicker').yearpicker();
});

