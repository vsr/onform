function submit_registration_form(e) {
    $('#registration-form').off('submit');
    $('#registration-form').submit(function(e) {
        e.preventDefault();
        let data = new FormData();
        let name, val;
        $('input[name]').each(function() {
            name = $(this).attr('name');
            val = $(this).val();

            data.append(name, val);
        });
        data.append('csrfmiddlewaretoken', $('input[name="csrfmiddlewaretoken"]').val());

        fetch('/api/submission', {
            method: 'POST',
            body: data
        })
        .then(response => {
            if (!response.ok) {
                 response.json().then(error_list => {
                     append_error_list(error_list);
                     throw new Error('Missing inputs.\n' + JSON.stringify(error_list));
                 });
            } else {
                window.location = '/success';
            }

            return false;
        });
    });
}

function append_error_list(error_list) {
    let $error_list = $('<ul id="error-list"></ul>');

    for(let key in error_list)
        $error_list.append('<li><strong>' + key + '</strong>' + ': ' + error_list[key] + '</li>');

    $('#error-list').remove();
    $("#submit-btn").before($error_list);
}

$(function() {
    $('[data-toggle="popover"]').popover({trigger: 'focus'});
    $('.twitter-typeahead').css('display','block');

    $('#submit-btn').on('click', function(e){
        submit_registration_form(e);
    });
});

