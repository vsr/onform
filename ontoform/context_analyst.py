import json
from django.apps import apps

CONTEXTS_DIR = apps.get_app_config('ontoform').CONTEXTS_DIR


class ContextAnalyst:
    def __init__(self):
        with open(CONTEXTS_DIR + 'contexts.json') as json_file:
            self.context_list = json.load(json_file)

    def suggest_o2f_ontologies(self, research_area, keywords, data_type):
        scores = []

        # attribute weights
        w_res_area = 1
        w_data_type = 1
        w_keywords = 1
        # with these weights, the score's value range will be 0<=x<=3

        number_of_keywords = len(keywords)
        threshold = 2  # => match at least 2 attributes entirely

        # calc score for each context
        for context in self.context_list:
            is_match_res_area = int(research_area in context['research_area'])
            is_match_data_type = int(data_type in context['data_type'])
            is_match_keywords = 0
            for keyword in keywords:
                is_match_keywords = is_match_keywords + int(keyword in context['keywords'])

            context_score = w_res_area * is_match_res_area + \
                            w_data_type * is_match_data_type + \
                            (w_keywords / number_of_keywords) * is_match_keywords

            if context_score >= threshold:
                scores.append(context['filename'])

        return scores
