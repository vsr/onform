import uuid
import rdflib
from rdflib.namespace import RDF, RDFS, XSD
from django.apps import apps

# directory with onf-descriptions
ONF_DESC_DIR = apps.get_app_config('ontoform').ONF_DESC_DIR

NAMESPACES = apps.get_app_config('ontoform').NAMESPACES

# OnForm Entities
ROOT_FORM_GROUP = rdflib.URIRef(NAMESPACES['onf_survey'] + 'RootFormGroup')

CLASS_FORM_GROUP = rdflib.URIRef(NAMESPACES['onf'] + 'FormGroup')
CLASS_INPUT_ELEMENT = rdflib.URIRef(NAMESPACES['onf'] + 'InputElement')

PROPERTY_HAS_NEXT = rdflib.URIRef(NAMESPACES['list'] + 'hasNext')
PROPERTY_HAS_CONTENTS = rdflib.URIRef(NAMESPACES['list'] + 'hasContents')
PROPERTY_IS_REPEATABLE = rdflib.URIRef(NAMESPACES['onf'] + 'isRepeatable')
PROPERTY_HEADING = rdflib.URIRef(NAMESPACES['onf'] + 'groupHeading')
PROPERTY_INPUT_HELP = rdflib.URIRef(NAMESPACES['onf'] + 'helpText')
PROPERTY_INPUT_LABEL = rdflib.URIRef(NAMESPACES['onf'] + 'elementLabel')
PROPERTY_RESOURCE = rdflib.URIRef(NAMESPACES['onf'] + 'resource')
PROPERTY_TYPE_OF = rdflib.URIRef(NAMESPACES['onf'] + 'typeof')
PROPERTY_PROPERTY = rdflib.URIRef(NAMESPACES['onf'] + 'property')
PROPERTY_REV = rdflib.URIRef(NAMESPACES['onf'] + 'rev')
PROPERTY_VOCAB = rdflib.URIRef(NAMESPACES['onf'] + 'vocab')


class FormBuilder:
    def __init__(self, filename, file_format='n3'):
        self.filename = filename
        self.graph = rdflib.Graph()
        self.graph.parse(ONF_DESC_DIR + filename, format=file_format)
        self.is_col_set = False

    def get_label(self, node):
        return self.graph.value(node, RDFS.label)

    def get_has_next(self, node):
        return self.graph.value(node, PROPERTY_HAS_NEXT)

    def get_is_repeatable(self, node):
        return self.graph.value(node, PROPERTY_IS_REPEATABLE)

    def get_has_contents(self, node):
        return self.graph.value(node, PROPERTY_HAS_CONTENTS)

    def get_root_form_group(self):
        if (ROOT_FORM_GROUP, PROPERTY_HAS_CONTENTS, None) in self.graph:
            return ROOT_FORM_GROUP
        return None

    def get_heading(self, node):
        return self.graph.value(node, PROPERTY_HEADING)

    def get_input_help(self, node):
        return self.graph.value(node, PROPERTY_INPUT_HELP)

    def get_input_label(self, node):
        return self.graph.value(node, PROPERTY_INPUT_LABEL)

    def get_input_type(self, node):
        node_range = self.graph.value(node, RDFS.range)

        if node_range == XSD.string or \
                node_range == XSD.language:
            return 'text'
        elif node_range == XSD.int or \
                node_range == XSD.integer:
            return 'number'
        elif node_range == XSD.date:
            return 'date'

        return None

    def get_input_range_xsd(self, node):
        node_range = self.graph.value(node, RDFS.range)
        return node_range

    def get_resource(self, node):
        return self.graph.value(node, PROPERTY_RESOURCE)

    def get_property(self, node):
        return self.graph.value(node, PROPERTY_PROPERTY)

    def get_type_of(self, node):
        return self.graph.value(node, PROPERTY_TYPE_OF)

    def get_rev(self, node):
        return self.graph.value(node, PROPERTY_REV)

    def get_vocab(self, node):
        return self.graph.value(node, PROPERTY_VOCAB)

    def is_form_group(self, node):
        if (node, RDF.type, CLASS_FORM_GROUP) in self.graph:
            return True
        return False

    def is_input_element(self, node):
        if (node, RDF.type, CLASS_INPUT_ELEMENT) in self.graph:
            return True
        return False

    def create_html(self, node):
        html = ''
        # end of node list?
        if node is None:
            return html

        # get next node
        next_node = self.get_has_next(node)
        # get node content
        node_content = self.get_has_contents(node)

        # get attributes
        node_vocab = self.get_vocab(node)
        node_resource = self.get_resource(node)
        node_property = self.get_property(node)
        node_type_of = self.get_type_of(node)
        node_rev = self.get_rev(node)
        node_heading = self.get_heading(node)
        node_input_label = self.get_input_label(node)
        node_input_help = self.get_input_help(node)
        node_input_type = self.get_input_type(node_property)
        node_input_range_xsd = self.get_input_range_xsd(node_property)

        # uuid is used for input fields && 
        # if "typeof" is given, but "resource" is missing
        node_id = uuid.uuid4()

        # start form group
        if self.is_form_group(node):
            # h2 heading for root
            if node_heading:
                if node is ROOT_FORM_GROUP:
                    html = html + '<h1 id="root" ontology-name="' + self.filename + '">' + str(node_heading) + '</h1>'
                    html = html + '<hr>'
            # add classes
            html = html + '<div class="'
            if node is ROOT_FORM_GROUP:
                html = html + 'root-form-group form-group row '
            elif self.is_col_set is False:  # assign first div after row to class "col"
                html = html + 'col '
                self.is_col_set = True
            if self.get_is_repeatable(node):
                html = html + 'o2f-repeatable '
            html = html + 'o2f-group" '
            # add o2f-group-name
            html = html + 'o2f-group-name="' + str(node) + '" '

        # create input element
        elif self.is_input_element(node):
            # label
            if self.is_col_set is False:  # assign first div after row to class "col"
                html = html + '<div class="form-group row col">'
                self.is_col_set = True
            else:
                html = html + '<div class="form-group row">'
            html = html + '<label for="' + str(node_id) + '" class="input-label form-label ' + \
                   'col-form-label col-sm-3">' + str(node_input_label) + '</label>'
            # input field
            html = html + '<div class="col-sm-8">' + '<input id="' + str(node_id) + '"' + 'class="form-control"'
            if node_input_type:
                html = html + ' type="' + node_input_type + '"'
            if node_input_range_xsd:
                html = html + ' datatype="' + str(node_input_range_xsd) + '"'

        if node_vocab:
            html = html + ' vocab="' + str(node_vocab) + '"'
        if node_resource:
            html = html + ' resource="' + str(node_resource) + '"'
        if node_property:
            html = html + ' property="' + str(node_property) + '"'
        if node_type_of:
            html = html + ' typeof="' + str(node_type_of) + '"'
        if node_rev:
            html = html + ' rev="' + str(node_rev) + '"'

        # create new instance
        if node_resource is None and node_type_of is not None:
            html = html + ' resource="' + \
                   NAMESPACES['onf_survey'] + \
                   str(node_id) + \
                   '"'

        # close div
        html = html + '>'

        # heading
        if node_heading and node is not ROOT_FORM_GROUP:
            html = html + '<div class="row"><h2>' + str(node_heading) + '</h2></div>'

        if self.is_input_element(node):
            # help button
            html = html + '</div>' + \
                   '<div class="col-sm-1">' + \
                   '<button type="button" class="help-button"' + \
                   ' data-container="body" data-toggle="popover"' + \
                   'data-placement="right" data-content="' + \
                   str(node_input_help) + '"><i class="fa fa-question-circle"></i>' + \
                   '</button></div>'

        if node_content:
            html = html + self.create_html(node_content) + '</div>'
        else:
            html = html + '</div>'

        return html + self.create_html(next_node)

    def create_repeatable(self, uri):
        repeatable_form_group_node = rdflib.URIRef(uri)
        return self.create_html(repeatable_form_group_node)

    def create_onto_form(self):
        root = self.get_root_form_group()
        result = self.create_html(root)

        return result
