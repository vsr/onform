from django.db import models


class ResearchData(models.Model):
    # citational, administrative information
    identifier = models.URLField()
    creator = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=2000)
    license = models.CharField(max_length=50)
    publisher = models.CharField(max_length=50)
    publication_year = models.IntegerField()

    research_area = models.URLField()
    keywords = models.CharField(max_length=2000) # comma separated
    data_type = models.CharField(max_length=50)
    # domain-specific information
    o2f_file = models.FileField()
