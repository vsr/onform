from django.views.generic import TemplateView


class Law(TemplateView):

    def get_template_names(self):
        path = self.request.path
        if path == '/':
            return 'ontoform/index.html'
        elif path == '/datenschutz':
            return 'ontoform/datenschutz.html'
        elif path == '/impressum':
            return 'ontoform/impressum.html'
