from django.views.generic import TemplateView


class Success(TemplateView):
    template_name = 'ontoform/success.html'

