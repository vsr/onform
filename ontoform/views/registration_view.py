from django.views.generic import TemplateView


class ResearchDataRegistration(TemplateView):
    template_name = 'ontoform/registration.html'

    def get_context_data(self, **kwargs):
        context = {
            'citational_information': [
#                {
#                    'id': 'identifier',
#                    'type': 'text',
#                    'label': 'Identifier (optional)',
#                    'comment': 'If available, please provide an identifier that has been assigned to the research data, e.g. a DOI'
#                },
                {
                    'id': 'title',
                    'type': 'text',
                    'label': 'Title',
                    'comment': 'A title that briefly describes the research data.'
                },
                {
                    'id': 'creator',
                    'type': 'text',
                    'label': 'Creator',
                    'comment': 'The name of the person(s) who created the research data.'
                },
                {
                    'id': 'description',
                    'type': 'text',
                    'label': 'Description',
                    'comment': 'A brief description of what the research data is about.'
                },
                {
                    'id': 'license',
                    'type': 'text',
                    'label': 'License',
                    'comment': 'If applicable, the license for the research data.'
                },
                {
                    'id': 'publisher',
                    'type': 'text',
                    'label': 'Publisher (optional)',
                    'comment': 'If applicable, the publisher of the research data.'
                },
                {
                    'id': 'publicationYear',
                    'type': 'date',
                    'label': 'Year of Publication',
                    'comment': 'The year the research data was first published.'
                }
            ]
        }
        return context
