import uuid

from rest_framework.views import APIView
from rest_framework.response import Response

from ontoform.forms.submission_form import SubmissionForm
from ontoform.models import ResearchData
from ontoform.tools.rdf_helper import RDFHelper

from django.apps import apps

SUBMISSION_DIR = apps.get_app_config('ontoform').SUBMISSION_DIR


class SubmissionAPI(APIView):

    def post(self, request):
        # validate form
        submission_form = SubmissionForm(
            {
#                'identifier': request.POST.get('identifier', ''),
                'title': request.POST['title'],
                'creator': request.POST['creator'],
                'description': request.POST['description'],
#                'license': request.POST.get('license', ''),
#                'publisher': request.POST.get('publisher', ''),
                'publicationYear': request.POST['publicationYear'],
                'research_area': request.POST.get('research_area', ''),
                'data_type': request.POST.get('data_type', ''),
                'keywords': request.POST.get('keywords', ''),
            }
        )

        # if valid, create objects
        if submission_form.is_valid():
            cleaned_data = submission_form.cleaned_data

            identifier = request.POST.get('identifier', '')
            o2f_data = request.POST.get('o2f_description', '')

            if identifier == '':
                identifier = 'dataset_' + str(uuid.uuid4())

            o2f_ttl = RDFHelper.extract_rdfa_from_html(o2f_data)
            research_context = {
                'research_area': cleaned_data['research_area'],
                'data_type': cleaned_data['data_type'],
                'keywords': cleaned_data['keywords'].split(','),
            }

            if o2f_ttl is None:
                o2f_ttl = RDFHelper.create_context_file(identifier, research_context)
            else:
                o2f_ttl = RDFHelper.add_research_context(identifier, research_context, o2f_ttl)

            o2f_filename = 'o2f_' + identifier + '.ttl'
            with open(SUBMISSION_DIR + o2f_filename, 'w', encoding='utf-8') as f:
                f.write(o2f_ttl.decode('utf-8'))

            ResearchData.objects.get_or_create(
                identifier=identifier,
                creator=cleaned_data['creator'],
                title=cleaned_data['title'],
                description=cleaned_data['description'],
#                license=cleaned_data['license'],
#                publisher=cleaned_data['publisher'],
                publication_year=cleaned_data['publicationYear'],
                o2f_file=o2f_filename
            )

            return Response(status=200)
        else:
            return Response(status=422,
                            data=submission_form.errors)
