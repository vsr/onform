from django.shortcuts import render
from django.views.generic import TemplateView

from ontoform.context_analyst import ContextAnalyst
from ontoform.form_builder import FormBuilder


class FormBuilderAPI(TemplateView):
    template_name = 'ontoform/generic.html'

    def post(self, request):
        # context data
        research_area = request.POST.get('research_area', '')
        data_type = request.POST.get('data_type', '')
        keywords = request.POST.get('keywords', '').split(',')

        context_analyst = ContextAnalyst()
        o2f_ontologies = context_analyst.suggest_o2f_ontologies(research_area, keywords, data_type)

        content = ""
        for o2f_ontology in o2f_ontologies:
            if o2f_ontology:
                content = content + FormBuilder(o2f_ontology).create_onto_form()

        context = {
            'generic_html': content
        }

        return render(request, self.template_name, context)
