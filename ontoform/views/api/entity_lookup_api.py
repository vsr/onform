from SPARQLWrapper import SPARQLWrapper, JSON, BASIC
from rest_framework.views import APIView
from rest_framework.response import Response
from django.apps import apps


def dbpedia_full_text_search(label, result_format):
    # spaces are not allowed in bif:contains
    label = label.replace(" ", "_")
    query_string = """PREFIX bif: <bif:>
    SELECT DISTINCT ?uri ?label ?description WHERE {
        ?uri rdfs:label ?label.
        ?uri rdfs:comment ?description.
        ?label bif:contains "%s".
        filter langMatches(lang(?label), "en").
        filter langMatches(lang(?description), "en").
    }""" % label
    return perform_sparql_query(apps.get_app_config('ontoform').SPARQL_ENDPOINTS['dbpedia'], query_string, result_format)


def wikidata_full_text_search(label, result_format):
    query_string = """SELECT DISTINCT ?uri ?label ?description WHERE {
        SERVICE wikibase:mwapi {
            bd:serviceParam wikibase:endpoint "www.wikidata.org";
                            wikibase:api "EntitySearch";
                            mwapi:search "%s";
                            mwapi:language "en".
            ?uri wikibase:apiOutputItem mwapi:item.
        }
        ?uri rdfs:label ?label.
        ?uri schema:description ?description.
        filter langMatches(lang(?label), "en").
        filter langMatches(lang(?description), "en").
    }""" % label
    return perform_sparql_query(apps.get_app_config('ontoform').SPARQL_ENDPOINTS['wikidata'], query_string, result_format)


def wordnet_search(label, result_format):
    label = label.replace(' ', ' AND ')
    query_string = """SELECT DISTINCT ?uri ?label ?description WHERE {
        ?uri <http://www.w3.org/ns/lemon/ontolex#sense> ?sense_node .
        ?sense_node <http://www.w3.org/ns/lemon/ontolex#isLexicalizedSenseOf> ?id_node.
        ?id_node <http://wordnet-rdf.princeton.edu/ontology#definition> ?definition_node.
        ?id_node <http://wordnet-rdf.princeton.edu/ontology#partOfSpeech> <http://wordnet-rdf.princeton.edu/ontology#noun> .
        ?definition_node <http://www.w3.org/1999/02/22-rdf-syntax-ns#value> ?description .
        ?uri <http://www.w3.org/ns/lemon/ontolex#canonicalForm> ?canonical_node .
        ?canonical_node <http://www.w3.org/ns/lemon/ontolex#writtenRep> ?label .
        (?label ?score) <tag:stardog:api:property:textMatch> "%s"
    }""" % label
    return perform_sparql_query(apps.get_app_config('ontoform').SPARQL_ENDPOINTS['wordnet'], query_string, result_format, 'anonymous', 'anonymous')


def perform_sparql_query(sparql_endpoint, query_string, result_format, username='', password=''):
    sparql = SPARQLWrapper(sparql_endpoint)
    sparql.setQuery(query_string)
    sparql.setReturnFormat(result_format)

    if username and password:
        sparql.setHTTPAuth(BASIC)
        sparql.setCredentials(username, password)

    result = sparql.query().convert()
    return result


class EntityLookupAPI(APIView):

    def get(self, request):
        label = request.GET['label']

        # remove multiple whitespace characters and put one space instead
        label = ' '.join(label.split())

        result_query = wordnet_search(label, JSON)
        return Response(result_query)
