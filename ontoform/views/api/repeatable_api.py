from django.views.generic import TemplateView
from ontoform.form_builder import FormBuilder


class RepeatableAPI(TemplateView):
    template_name = 'ontoform/generic.html'

    def get_context_data(self, **kwargs):
        uri = self.request.GET['uri']
        ontology_name = self.request.GET['ontology_name']
        form_builder = FormBuilder(ontology_name)
        form_builder.is_col_set = True

        context = {
            'generic_html': form_builder.create_repeatable(uri)
        }

        return context
