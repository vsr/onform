from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from ontoform.views.registration_conventional_view import ResearchDataRegistrationConventional
from ontoform.views.registration_view import ResearchDataRegistration
from ontoform.views.api.submission_api import SubmissionAPI
from ontoform.views.api.entity_lookup_api import EntityLookupAPI
from ontoform.views.api.form_builder_api import FormBuilderAPI
from ontoform.views.api.repeatable_api import RepeatableAPI
from ontoform.views.success_view import Success
from ontoform.views.law_view import Law

urlpatterns = [
    # for survey only
    path('', Law.as_view(), name='index'),
    path('success', Success.as_view(), name='success'),
    path('datenschutz', Law.as_view(), name='datenschutz'),
    path('impressum', Law.as_view(), name='impressum'),
    path('registrationConventional', ResearchDataRegistrationConventional.as_view(), name='registrationConventional'),

    path('registration', ResearchDataRegistration.as_view(), name='registration'),
    path('api/buildForm', csrf_exempt(FormBuilderAPI.as_view()), name='buildForm'),
    path('api/repeatable', RepeatableAPI.as_view(), name='repeatable'),
    path('api/entityLookup', EntityLookupAPI.as_view(), name='entityLookup'),
    path('api/submission', SubmissionAPI.as_view(), name='submission'),
]
