# Generated by Django 3.0.6 on 2020-06-02 23:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ontoform', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='keyword',
            name='name',
        ),
        migrations.RemoveField(
            model_name='license',
            name='identifier',
        ),
        migrations.RemoveField(
            model_name='researcharea',
            name='name',
        ),
        migrations.RemoveField(
            model_name='researchdata',
            name='keyword',
        ),
        migrations.RemoveField(
            model_name='researchdata',
            name='rdf_file',
        ),
        migrations.RemoveField(
            model_name='researchdata',
            name='research_context',
        ),
        migrations.RemoveField(
            model_name='researchdata',
            name='resource_type',
        ),
    ]
