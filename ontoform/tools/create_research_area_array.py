# tool to create a sorted JSON array of research areas
# required input: Basisklassifikation as RDF (basisklassifikation.nt)
import json
from django.apps import apps
from SPARQLWrapper import SPARQLWrapper, JSON


def get_research_areas_as_array():
    sparql = SPARQLWrapper(apps.get_app_config('ontoform').SPARQL_ENDPOINTS['basisklassifikation'])
    sparql.setCredentials('anonymous', 'anonymous')
    query = """select ?main_class ?sub_class ?label where {
        {
            ?main_class a <http://www.w3.org/2004/02/skos/core#Concept> .
            ?main_class <http://www.w3.org/2004/02/skos/core#prefLabel> ?label .
        } union {
            ?main_class <http://www.w3.org/2004/02/skos/core#narrower> ?sub_class .
            ?sub_class <http://www.w3.org/2004/02/skos/core#prefLabel> ?label .
        }
        filter(langMatches(lang(?label), 'en'))
    }
    """
    sparql.setReturnFormat(JSON)
    sparql.setQuery(query)
    results = sparql.query().convert()
    classes = {}

    for result in results['results']['bindings']:
        if 'sub_class' not in result:
            main_class_uri = result['main_class']['value']
            main_class_label = result['label']['value']
            classes.update({
                main_class_uri: {
                    'label': main_class_label,
                    'sub_classes': []
                }
            })

    for result in results['results']['bindings']:
        if 'sub_class' in result:
            main_class_uri = result['main_class']['value']
            sub_class_uri = result['sub_class']['value']
            sub_class_label = result['label']['value']
            classes[main_class_uri]['sub_classes'].append(
                {
                    'uri': sub_class_uri,
                    'label': sub_class_label
                }
            )

    classes_sorted = []
    for main_key in classes:
        main_class = classes[main_key]
        classes_sorted.append(
            {
                'uri': main_key,
                'label': main_class['label']
            }
        )
        for sub_class in main_class['sub_classes']:
            classes_sorted.append(sub_class)
    return json.dumps(classes_sorted)


print(get_research_areas_as_array())
