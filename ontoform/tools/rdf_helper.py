import rdflib
from rdflib import URIRef, Literal, RDF
from rdflib.plugin import register, Parser


def create_uriref_or_literal(thing):
    if thing.startswith('http'):
        return URIRef(thing)
    else:
        return Literal(thing)


class RDFHelper:
    @staticmethod
    def create_context(identifier, research_context):
        research_area = create_uriref_or_literal(research_context['research_area'])
        data_type = create_uriref_or_literal(research_context['data_type'])
        keywords = research_context['keywords']

        g = rdflib.Graph()
        uri = URIRef('https://vsr.informatik.tu-chemnitz.de/edu/studentprojects/2019/025/#' + identifier)

        # add research context
        g.add([uri, URIRef('https://vsr.informatik.tu-chemnitz.de/edu/studentprojects/2019/025/#hasResearchArea'),
               research_area])
        g.add([uri, URIRef('https://vsr.informatik.tu-chemnitz.de/edu/studentprojects/2019/025/#hasDataType'), data_type])
        for keyword in keywords:
            keyword = create_uriref_or_literal(keyword)
            g.add([uri, URIRef('https://vsr.informatik.tu-chemnitz.de/edu/studentprojects/2019/025/#relatedTo'), keyword])

        return g

    @staticmethod
    def create_context_file(identifier, research_context, target_format='turtle'):
        context_graph = RDFHelper.create_context(identifier, research_context)
        serialised = context_graph.serialize(format=target_format)
        return serialised.decode('utf-8')

    @staticmethod
    def add_research_context(identifier, research_context, ttl_data, target_format='turtle'):
        research_area = create_uriref_or_literal(research_context['research_area'])
        data_type = create_uriref_or_literal(research_context['data_type'])
        keywords = research_context['keywords']

        g = rdflib.Graph()
        g.parse(data=ttl_data, format='turtle')
        context_graph = RDFHelper.create_context(identifier, research_context)
        g = g + context_graph

        serialised = g.serialize(format=target_format)
        return serialised

    @staticmethod
    def extract_rdfa_from_html(html_data, target_format='turtle'):
        g = rdflib.Graph()
        register('html', Parser, 'rdflib.plugins.parsers.structureddata', 'StructuredDataParser')
        g.parse(data=html_data, format='html')

        if len(g) <= 1:
            return None
        else:
            # remove triples with empty object
            for s, p, o in g:
                if str(o) == '':
                    g.remove((s, p, o))

            serialised = g.serialize(format=target_format)
            return serialised
